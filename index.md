---
title: About me
---

I’m a senior student at the [University of São Paulo (USP)](https://www5.usp.br),
attending the Bachelor’s degree in Computer Science course. Currently, I’m at
the end of a one year undergraduate research in High-Performance Computing. The
goal of this project was to accelerate astrophysical software for black hole
studies using GPUs. Also, I’m working as a teaching assistant on IME-USP’s
Concurrent and Parallel Programming course, giving lectures and
developing/grading programming assignments. Besides parallel and
high-performance computing I’m very passionate about software development in
general, but especially low-level coding, and FLOSS. I got the amazing
oportunity to be part of GSoC 2019, working at Git! Check it out
[here]({% link gsoc.md %}).

Find me as `matheustavares` on [GitLab](https://gitlab.com/matheustavares),
[GitHub](https://github.com/matheustavares) and IRC (`#git-devel@freenode` or
`#ccsl-usp@freenode`).
