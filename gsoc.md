---
title: GSoC
category_filter: gsoc
permalink: "gsoc"
---

I'm participating in
[GSoC 2019](https://summerofcode.withgoogle.com/projects/#6477677521797120)
contributing to Git. This page is intended to contain weekly updates in my
project.You may also want to read my
[project proposal]({{ site.url }}{% link assets/Matheus_Tavares_GSoC_Proposal.pdf %}).

<ul class="list pa0">
  {% for post in site.posts %}
  {% if post.categories contains page.category_filter %}
  <li class="mv2">
    <a href="{{ site.url }}{{ post.url }}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ttu">{{ post.date | date_to_string }} </time>
      {{ post.title }}
    </a>
  </li>
  {% endif %}
  {% endfor %}
</ul>
