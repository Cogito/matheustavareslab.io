---
title: Warming Up
categories: gsoc
---

It's been a little over a week since GSoC's projects were announced. During
this initial time I've been working mainly on two things:

1. Setting up this webpage where I'll weekly post updates on my project.
2. A patch on Git's interactive rebase (`git rebase -i`). This is not directly
related to my GSoC project, but it's something I've been wanting to do for a
while now. And it's a good warmup :) You can see the result
[here](https://github.com/matheustavares/git/commit/901c8f021c82ae612c05fbab5f77f30942c4bfc7).

As there isn't much to talk about the first item, let's explore the second a
little.

## Context

When you run an interactive rebase, Git opens a todo list containing the commits
to be rebased. It's them possible to choose between a variety of commands to be
performed for each commit on the list.

Among the available commands we have `reword`, which opens an editor to amend
the commit's message and `squash`, which joins the commit with the previous one,
also opening the editor for message editing.

## The Patch

The idea behind this patch is to display a commit diff in the editor opened
during a `reword` or `squash` to help users better describe the changes
included in each commit. This was achieved throught the addition of a new
configuration option called `rebase.showDiffOnCommit`. If set to `true` it will
make the interactive rebase backend use the `-v` option when invoking
`git commit`, which, in turn, will display the diff. This patch also includes
some tests for this new feature and documentation about the added configuration.

## Difficulties

What took me longer in this patch was understanding the code and planning. For
example, what would be the best way to expose the new feature to the users:
throught a CLI option or a configuration entry? In this case, I ended up
implementing both but choosing to keep just the latter as it seemed more
"natural".

Another task that required me some time was to add tests. Besides `lib-rebase`'s
`set_fake_editor()` function, little did I know about how to fake editors on
Git tests. Also, it was trick to think how to set **one** fake editor to
automatically perform the **two** required tasks:

1. replace some `pick`'s for `reword`'s and `squash`'s at the TODO list when Git
firstly invoked the editor;
2. Check for the diff when it opened the editor for a second time, with the
commit message.

But reading other test files and the implementation of `set_fake_editor()` I
managed to understand what was needed. In the end, it was a very nice learning
experience.

## Next steps

My plan for the "next sprint", is to investigate some more function call chains
throught the pack access code and start working on `sha1-file.c`, where some of
these functions reside. In my initial schedule, I designated these first weeks
of community bonding to begin protecting `sha1-file.c`'s global states,
starting by the object cache. But, even though I've been thinking about it, I'm
not sure yet what is the best way to do it. So I'll probably invest
some more time, in the next week, reading the code and investigating.
