# Matheus Tavares' Personal Page

This is my page :) Visit it at
[https://matheustavares.gitlab.io](https://matheustavares.gitlab.io).

It was based on an amazing jekyll template called
[Scribble](https://github.com/muan/scribble).

## Found errors?

If you find any problem in my page, please,
[open an issue](https://gitlab.com/MatheusTavares/matheustavares.gitlab.io/issues).

## Running

1. Run `bundle install`

2. Run Jekyll: `bundle exec jekyll serve -w`

3. Go to http://localhost:4000
